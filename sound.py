'''Programa que al ser ejecutado en la terminal hace uso de los programas sources,
processors y sinks con las variables escritas por el usuario'''

import sys

import sources
import processors
import sinks

def main():
    #source
    sys.argv.pop(0)
    source = sys.argv.pop(0)
    if source == 'load':
        sound = sources.load(path=sys.argv.pop(0))
    elif source == 'sin':
        sound = sources.sin(nsamples=int(sys.argv.pop(0)),freq=float(sys.argv.pop(0)))
    elif source == 'constant':
        sound = sources.constant(nsamples=int(sys.argv.pop(0)),level=int(sys.argv.pop(0)))
    elif source == 'square':
        sound = sources.square(nsamples=int(sys.argv.pop(0)),nperiod=int(sys.argv.pop(0)))
    #processor
    processor = sys.argv.pop(0)
    if processor == 'ampli':
        sound = processors.ampli(sound, factor=float(sys.argv.pop(0)))
    elif processor == 'shift':
        sound = processors.shift(sound, value=int(sys.argv.pop(0)))
    elif processor == 'trim':
        sound = processors.trim(sound, reduction=int(sys.argv.pop(0)), start=bool(sys.argv.pop(0)))
    elif processor == 'repeat':
        sound = processors.repeat(sound, factor=int(sys.argv.pop(0)))
    #sink
    sink = sys.argv.pop(0)
    if sink == 'play':
        sinks.play(sound)
    elif sink == 'draw':
        sinks.draw(sound, max_chars=int(sys.argv.pop(0)))
    elif sink == 'show':
        sinks.show(sound, newline=bool(sys.argv.pop(0)))
    elif sink == 'info':
        sinks.info(sound)



if __name__ == '__main__':
    main()