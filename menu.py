
import sources
import sinks
def main ():
    sound = []
    source = True

    while source == True:
        print("Dime una fuente (load, sin, constant, square):")
        sour = input()
        if sour == 'load':
            print('Dame los parametros para load.')
            m_path = input('Path: ')
            sound = sources.load(path=m_path)
            f_source = 'load'
            parameters = m_path
            source = False
        elif sour == 'sin':
            print('Dame los parametros para sin.')
            m_nsamples = int(input('nsamples: '))
            m_freq = float(input('freq: '))
            sound = sources.sin(nsamples=m_nsamples, freq=m_freq)
            f_source = 'sin'
            parameters = (m_nsamples, m_freq)
            source = False
        elif sour == 'constant':
            print('Dame los parametros para constant.')
            m_nsamples = int(input('nsamples: '))
            m_level= int(input('level: '))
            sound = sources.constant(nsamples=m_nsamples, level=m_level)
            f_source = 'constant'
            parameters = (m_nsamples, m_level)
            source = False
        elif sour == 'square':
            print('Dame los parametros para square.')
            m_nsamples = int(input('nsamples: '))
            m_nperiod = int(input('nperiod: '))
            sound = sources.square(nsamples=m_nsamples, nperiod=m_nperiod)
            f_source = 'square'
            parameters = (m_nsamples, m_nperiod)
            source = False
    sumidero = True
    while sumidero == True:
        print('Dime un sumidero (play, draw, show, info):')
        sink = input()
        if sink == 'play':
            print(f'Ejecutando {f_source} {parameters} y {sink}()')
            sinks.play(sound)
            sumidero = False
        elif sink == 'draw':
            print('Dame los parametros para draw.')
            m_chart = int(input('max_charts: '))
            print(f'Ejecutando {f_source} {parameters} y {sink}()')
            sinks.draw(sound, max_chars=m_chart)
            sumidero = False
        elif sink == 'show':
            print('Dame los parametros para show:')
            m_line = input('newline: ')
            if m_line == 'True':
                m_line = bool(1)
                print(f'Ejecutando {f_source} {parameters} y {sink}()')
                sinks.show(sound, newline=m_line)
                t_line = True
                sumidero = False
            elif m_line == 'False':
                m_line = bool(0)
                print(f'Ejecutando {f_source} {parameters} y {sink}()')
                sinks.show(sound, newline=m_line)
                t_line = True
                sumidero = False
        elif sink == 'info':
            print(f'Ejecutando {f_source} {parameters} y {sink}()')
            sinks.info(sound)
            sumidero = False






if __name__ == '__main__':
    main()