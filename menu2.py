
import sources
import sinks

def main ():

    sound = []
    source = True
    while source == True:
        print("Dime una fuente (load, sin, constant, square):")
        sour = input()
        if sour == 'load':
            print('Dame los parametros para load.')
            correct = False
            while not correct:
                m_path = input('Path: ')

                try:
                    sound = sources.load(path=m_path)
                    f_source = 'load'
                    parameters = m_path
                    correct = True
                    source = False

                except:
                    print(f'Se ha intoducido ({m_path}) y no es un archivo disponible, escriba un archivo disponible.')
        elif sour == 'sin':
            s_correct = False
            print('Dame los parametros para sin.')
            while not s_correct:
                try:
                    m_nsamples = input('nsamples: ')
                    m_nsamples = int(m_nsamples)
                    f_correct = False
                    while not f_correct:
                        try:
                            m_freq = input('freq: ')
                            m_freq = float(m_freq)
                            sound = sources.sin(nsamples=m_nsamples, freq=m_freq)
                            f_source = 'sin'
                            parameters = (m_nsamples, m_freq)
                            f_correct = True
                            s_correct = True
                            source = False
                        except:
                            print(f'Freq deberia ser un numero real pero se ha escrito: ({m_freq})')
                except:
                    print(f'Nsamples deberia ser un numero entero pero se ha escrito ({m_nsamples})')
        elif sour == 'constant':
            print('Dame los parametros para constant.')
            s_correct = False
            while not s_correct:
                try:
                    m_nsamples = input('nsamples: ')
                    m_nsamples = int(m_nsamples)
                    l_correct = False
                    while not l_correct:
                        try:
                            m_level= input('level: ')
                            m_level = int(m_level)
                            sound = sources.constant(nsamples=m_nsamples, level=m_level)
                            f_source = 'constant'
                            parameters = (m_nsamples, m_level)
                            l_correct = True
                            s_correct = True
                            source = False
                        except:
                            print(f'Level deberia ser un numero entero pero se ha escrito ({m_level})')
                except:
                    print(f'Nsamples deberia ser un numero entero pero se ha escrito ({m_nsamples})')
        elif sour == 'square':
            print('Dame los parametros para square.')
            s_correct = False
            while not s_correct:
                try:
                    m_nsamples = input('nsamples: ')
                    m_nsamples = int(m_nsamples)
                    p_correct = False
                    while not p_correct:
                        try:
                            m_nperiod = input('nperiod: ')
                            m_nperiod = int(m_nperiod)
                            sound = sources.square(nsamples=m_nsamples, nperiod=m_nperiod)
                            f_source = 'square'
                            parameters = (m_nsamples, m_nperiod)
                            source = False
                            s_correct = True
                            p_correct = True
                        except:
                            print(f'Nperiod deberia ser un numero entero pero se ha escrito ({m_nperiod})')
                except:
                    print(f'Nsamples deberia ser un numero entero pero se ha escrito ({m_nsamples})')
        else:
            print(f"Se ha escrito ({sour}), pero debería ser una de las opciones de fuente dadas.")
    sumidero = True
    while sumidero == True:
        print('Dime un sumidero (play, draw, show, info):')
        sink = input()
        if sink == 'play':
            print(f'Ejecutando {f_source} {parameters} y {sink}()')
            sinks.play(sound)
            sumidero = False
        elif sink == 'draw':
            print('Dame los parametros para draw.')
            t_chart = False
            while not t_chart:
                try:
                    m_chart = input('max_charts: ')
                    m_chart = int(m_chart)
                    print(f'Ejecutando {f_source} {parameters} y {sink}()')
                    sinks.draw(sound, max_chars=m_chart)
                    t_chart = True
                    sumidero = False
                except:
                    print(f'Max_charts tiene que ser un entero y se ha escrito {m_chart}')
        elif sink == 'show':
            print('Dame los parametros para show:')
            t_line = False
            while not t_line:
                    m_line = input('newline: ')
                    if m_line == 'True':
                        m_line = bool(1)
                        print(f'Ejecutando {f_source} {parameters} y {sink}()')
                        sinks.show(sound, newline=m_line)
                        t_line = True
                        sumidero = False
                    elif m_line == 'False':
                        m_line = bool(0)
                        print(f'Ejecutando {f_source} {parameters} y {sink}()')
                        sinks.show(sound, newline=m_line)
                        t_line = True
                        sumidero = False
                    else:
                        print(f'Newline tiene que ser True o False y se ha escrito {m_line}')
        elif sink == 'info':
            print(f'Ejecutando {f_source} {parameters} y {sink}()')
            sinks.info(sound)
            sumidero = False
        else:
            print(f"Se ha escrito ({sink}), pero debería ser una de las opciones de sumidero dadas.")






if __name__ == '__main__':
    main()