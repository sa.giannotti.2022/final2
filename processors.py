"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config

def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """


    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        sound[nsample] = int(value)
    return sound

def shift (sound, value: int):
    '''Suma una cantidad (value) a todos los elementos de la lista'''
    for i in range(len(sound)):
        sound[i] += value
        if sound[i]>config.max_amp:
            sound[i] = config.max_amp

    return sound

def trim(sound, reduction: int, start: bool):
    '''Elimina de la lista la cantidad indicada por reduction, del principio o el final
    dependiendo de start'''
    sound2 = []
    sound2.extend(sound)
    if reduction > len(sound):
        sound = []
    else:
        if start == True:
            for i in range(reduction):
                sound.remove(sound2[i])
        else:
            for j in range(reduction+1):
                if j>0:
                    sound.remove(sound2[-j])

    return sound

def repeat(sound, factor: int):
    '''Crea una lista con la cantidad de copias que indique el factor sumada a la original'''
    sound *= (factor+1)
    return sound

'''PARTE OPCIONAL'''

def clean(sound, level: int):

    for i in range(len(sound)):
        if sound[i] < level and sound[i] > -level:
            sound[i] = 0

    return sound

def round(sound):

    sound2 = []
    sound2.extend(sound)

    for i in range(len(sound)):

        if i >= 1 and i < (len(sound)-1):
            sound[i] = (sound2[i-1]+sound2[i]+sound2[i+1])//3

    return sound

def add(sound1, sound2):

    sound = []
    if len(sound1) > len(sound2):
        sound.extend(sound1)
    elif len(sound1) < len(sound2):
        sound.extend(sound2)
    else:
        sound.extend(sound1)

    for i in range(len(sound)):
        if i > (len(sound2) - 1):
            sound[i] = sound1[i]
        elif i > (len(sound1) - 1):
            sound[i] = sound2[i]
        else:
            sound[i] = sound1[i] + sound2[i]
        if sound[i] > config.max_amp:
            sound[i] = config.max_amp
        elif sound[i] < -config.max_amp:
            sound[i] = -config.max_amp
    return sound

