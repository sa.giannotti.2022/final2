"""Methods for producing sound (sound sources)"""

import math

import soundfile

import config

def load(path: str):
    """Load sound from a file

    :param path: path of the file to read
    :return:     list of samples (sound)
    """

    data, fs = soundfile.read(path, dtype='int16')

    # Create q liwt of integers
    sound = [0] * len(data)
    # Convert list of int16 to list of int
    for nsample in range(len(data)):
        sound[nsample] = data[nsample][0]

    return sound

def sin(nsamples: int, freq: float):
    """Produce a list of samples of a sinusoidal signal (sound)

    :param nsamples: number of samples
    :param freq:     frequency of the signal, in Hz (cycles/sec)
    :return:         list of samples (sound)
    """

    # Create q liwt of integers
    sound = [0] * nsamples

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp *
                             math.sin(2 * config.pi * freq * t))

    return sound

def constant(nsamples: int, level: int):
    '''Produce una lista del tamaño de muestras con un valor constante'''

    if level > config.max_amp:
        level = config.max_amp
    elif level < -config.max_amp:
        level = -config.max_amp

    sound = [level] * nsamples

    return sound

def square(nsamples: int, nperiod: int):
    '''Produce una lista que crea una señal cuadrada entre los valores maximos de amplitud con un periodo introducido
    y del tamaño de la cantidad de muestras introducidas'''

    sound = []

    for i in range(nsamples):

        if i%nperiod <=((nperiod/2)-1):
            sound.append(config.max_amp)
        else:
            sound.append(-config.max_amp)

    return sound
