"""Methods for storing sound, or playing it"""

import array
import config

import sounddevice


def play(sound):
    sound_array = array.array('h', [0] * len(sound))
    for nsample in range(len(sound)):
        sound_array[nsample] = sound[nsample]
    sounddevice.play(sound_array, config.samples_second)
    sounddevice.wait()

def draw(sound, max_chars: int):
    total_chars = max_chars * 2
    for nsample in range(len(sound)):
        if sound[nsample] > 0:
            print(' ' * max_chars, end='')
            nchars = int(max_chars * (sound[nsample] / config.max_amp))
            print('*' * nchars)
        else:
            nchars = int(max_chars * (-sound[nsample] / config.max_amp))
            print(' ' * (max_chars - nchars), end='')
            print('*' * nchars)

def show(sound, newline: bool):
    '''Muestra todos los contenidos de la lista sound en distintas lineas
    o juntos separados por comas'''
    if newline == True:
        for i in range(len(sound)):
            print(sound[i])
    else:
        for i in range(len(sound)):
            if i == (len(sound)-1):
                print(f"{sound[i]}.")
            else:
                print(f"{sound[i]}, ", end="")

def info(sound):
    '''Muestra la cantidad de muestras, valor minimo, maximo, media, cantidad de muestras
    positivas, cantidad de muestras negativas y  muestras iguales a cero'''

    #samples
    print(f"Samples: {len(sound)}")
    #Max value
    max = 0
    min = 0
    sample_sum = 0
    pos_max = 0
    pos_min = 0

    for i in range(len(sound)):

        sample_sum += sound[i] #esto sirve para la media

        if sound[i] > max: #max value
            max = sound[i]
            pos_max = i

        if min > sound[i]: #min value
            min = sound[i]
            pos_min = i

    mean = sample_sum // len(sound)
    print(f"Max Value: {max} (sample {pos_max})")
    print(f"Min Value: {min} (sample {pos_min})")
    print(f"Mean value: {mean}")

    #positives, negatives y null
    positives = 0
    negatives = 0
    nulls = 0
    for l in sound:
        if l > 0:
            positives +=1
        elif l == 0:
            nulls += 1
        else:
            negatives += 1
    print(f"Positives samples: {positives}")
    print(f"Negative Samples: {negatives}")
    print(f"Null samples: {nulls}")